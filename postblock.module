<?php

/**
 * @file
 * Allows users to create blocks that display links to the various content types
 * on their site.
 */

/**
 * Implementation of hook_perm().
 */
function postblock_perm() {
  return array('administer postblock');
}

/**
 * Implementation of hook_theme().
 */
function postblock_theme() {
  return array(
    'postblock' => array(
      'arguments' => array('types' => array(), 'postblock_id' => 1),
      'template' => 'postblock',
    ),
    'postblock_types' => array(
      'arguments' => array('form' => array()),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function postblock_menu() {
  $items['admin/settings/postblock'] = array(
    'title' => 'Post block',
    'description' => 'Configure Post Block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('postblock_admin_settings'),
    'access arguments' => array('administer postblock'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Admin settings form
 */
function postblock_admin_settings() {
  $form['postblock_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of blocks'),
    '#description' => t('The number of Post Blocks you want. Must be between 1 and 99.'),
    '#default_value' => variable_get('postblock_number', 1),
    '#size' => 2,
    '#maxlength' => 2,
  );

  // Validate input
  $form['#validate'][] = 'postblock_settings_validate';

  // Build the form
  return system_settings_form($form);
}

/**
 * Validation for admin settings form
 */
function postblock_settings_validate($form, &$form_state) {
  $number = $form_state['values']['postblock_number'];

  // Make sure number is made up of only numeric characters
  if (!ctype_digit($number)) {
    form_set_error('postblock_number', t("'Number of blocks' must be a number."));
  }

  // Make sure number is between 1 & 99
  if ($number < 1 || $number > 99) {
    form_set_error('postblock_number', t("'Number of blocks' must be between 1 & 99 (inclusive)."));
  }
}

/**
 * Implementation of hook_block().
 */
function postblock_block($op = 'list', $delta = 0, $edit = array()) {
  $node_types = node_get_types('names');

  switch ($op) {
    case 'list':
      // Create as many blocks as entered in the settings form
      for ($i = 1; $i <= variable_get('postblock_number', 1); $i++) {
        $blocks[$i] = array(
          'info' => variable_get('postblock_name_' . $i, 'Post Block ' . $i) . ' (Post Block)',
          'cache' => BLOCK_CACHE_PER_ROLE,
        );
      }
      return $blocks;
    break;

    case 'configure':
      $form['postblock_name_' . $delta] = array(
        '#type' => 'textfield',
        '#title' => t('Block name'),
        '#description' => t('A custom name for the block to help distinguish from other Post Block blocks.') . '<br />' . t('NOTE: This name is only used on the Blocks administration page (admin/build/block) and is never shown in the block itself.'),
        '#default_value' => variable_get('postblock_name_' . $delta, 'Post Block ' . $delta),
      );
      $form['postblock_content_types'] = array(
        '#type' => 'fieldset',
        '#title' => t('Content types'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['postblock_content_types']['postblock_selection_' . $delta] = array(
        '#type' => 'radios',
        '#title' => t('Selection method'),
        '#description' => t('Show: Display only the selected content types. New content types will need to be enabled manually.') . '<br />' . t('Hide: Display all except the selected content types. New content types will be enabled automatically.'),
        '#default_value' => variable_get('postblock_selection_' . $delta, 0),
        '#options' => array(t('Show'), t('Hide')),
      );
      $form['postblock_content_types']['postblock_types_' . $delta] = array(
        '#tree' => TRUE,
        '#theme' => 'postblock_types',
      );
      $default = variable_get('postblock_types_' . $delta, array());
      foreach ($node_types as $id => $node_type) {
        $form['postblock_content_types']['postblock_types_' . $delta][$id]['data'] = array(
          '#type' => 'checkbox',
          '#title' => $node_type,
          '#default_value' => $default[$id]['data'],
        );
        $form['postblock_content_types']['postblock_types_' . $delta][$id]['weight'] = array(
          '#type' => 'weight',
          '#default_value' => $default[$id]['weight'],
          '#attributes' => array('class' => 'weight'),
        );
      }
      return $form;
    break;

    case 'save':
      variable_set('postblock_name_' . $delta, $edit['postblock_name_' . $delta]);
      variable_set('postblock_selection_' . $delta, $edit['postblock_selection_' . $delta]);
      variable_set('postblock_types_' . $delta, $edit['postblock_types_' . $delta]);
    break;

    case 'view':
      $block = array(
        'subject' => t('Post Content'),
        'content' => postblock_generate_block($delta),
      );
      return $block;
    break;
  }
}

/**
 * Theme the Post Block content types form
 */
function theme_postblock_types($form) {
  $table_rows = array();

  // Add each table row to array
  foreach (element_children($form) as $id) {
    $this_row = array();

    $this_row[] = drupal_render($form[$id]['data']);
    $this_row[] = drupal_render($form[$id]['weight']);

    $table_rows[] = array('data' => $this_row, 'class' => 'draggable');
  }

  // Output table
  $header = array('Content Types', 'Order');
  $output = theme('table', $header, $table_rows, array('id' => 'postblock-node-types'));
  $output .= drupal_render($form);
  $output .= '<div class="description">' . t("Content types selected here will be shown or hidden depending on the 'Selection method' above. The order of these content types represent the order the links will appear in the Post Block.") . '<br />' . t("NOTE: Content type links displayed in the Post Block will only be visible to users with the 'create [content type]' permission.") . '</div>';

  drupal_add_tabledrag('postblock-node-types', 'order', 'sibling', 'weight');

  return $output;
}

/**
 * Generate the contents of the block
 */
function postblock_generate_block($delta) {
  $types = variable_get('postblock_types_' . $delta, array());
  $method = variable_get('postblock_selection_' . $delta, 0);
  $all_types = node_get_types('types');
  $list = array();
  $weights = array();

  // Get the list of content types the current user has permission to create
  foreach ($types as $key => $type) {
    if ($type['data'] != $method && node_access('create', $key)) {
      $list[] = $all_types[$key];
      $weights[] = $type['weight'];
    }
  }

  // Theme list
  if (!empty($list)) {
    // Sort list in specified order
    array_multisort($weights, $list);

    return theme('postblock', $list, $delta);
  }
}

/**
 * Preprocess function for postblock.tpl.php
 */
function template_preprocess_postblock(&$variables) {
  // Get ID of last item
  $last = count($variables['types']) - 1;

  // Setup variables
  foreach ($variables['types'] as $key => $type) {
    $items[$key]->type = $type->type;
    $items[$key]->name = check_plain($type->name);

    // Remove HTML tags if any exist
    $description = strpos($type->description, '<') !== FALSE ? strip_tags($type->description) : $type->description;
    $items[$key]->description = check_plain($description);

    // Replace underscores with hyphens so links work properly
    $safe_type = str_replace('_', '-', $items[$key]->type);
    $items[$key]->url = url('node/add/' . $safe_type);

    // Set 'odd' or 'even' status
    $items[$key]->zebra = ($key % 2) ? 1 : 0;

    // Set 'first' & 'last' status
    $items[$key]->first = ($key == 0) ? TRUE : FALSE;
    $items[$key]->last = ($key == $last) ? TRUE : FALSE;

    // Set active status
    $trail = array_reverse(menu_get_active_trail());
    $items[$key]->active = ('node/add/' . $safe_type == $trail[0]['href']) ? TRUE : FALSE;
  }

  // Assign variables
  $variables['items'] = $items;

  // Setup template suggestions
  $variables['template_file'] = 'postblock-' . $variables['postblock_id'];
}
